import axios from 'axios'
import { store } from '../store/configureStore'
import { setWeatherApiError } from '../actions/home'

export default ({ lat, lng }, weatherApiKey, preferredUnits: string = 'us') =>
	axios
		.get(
			`https://api.darksky.net/forecast/${weatherApiKey}/${lat},${lng}?units=${preferredUnits}`
		)
		.then(res => res.data)
		.catch(() => {
			store.dispatch(setWeatherApiError(true))
		})
