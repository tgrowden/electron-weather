import React from 'react'
import { withStyles } from '@material-ui/core/styles'

const styles = (theme: MuiTheme) => ({
	arrowWrapper: {
		width: 'fit-content',
		display: 'inline-block',
		marginLeft: theme.spacing.unit / 2
	}
})

type Props = {
	classes: Object,
	bearing: number,
	arrowCharacter?: string,
	offset?: number
}

const BearingArrow = ({
	classes,
	bearing,
	arrowCharacter,
	offset,
	...props
}: Props) => {
	const calculatedBearing = bearing - offset

	return (
		<div
			className={classes.arrowWrapper}
			style={{ transform: `rotate(${calculatedBearing}deg)` }}
			{...props}
		>
			{arrowCharacter}
		</div>
	)
}

BearingArrow.defaultProps = {
	arrowCharacter: '↖',
	offset: 135
}

export default withStyles(styles)(BearingArrow)
