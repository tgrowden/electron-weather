# Electron Weather
[![pipeline status](https://gitlab.com/tgrowden/electron-weather/badges/master/pipeline.svg)](https://gitlab.com/tgrowden/electron-weather/commits/master)

A desktop weather app using React, Electron, Flow, Redux, Webpack, and Material-UI initialized with [Electron React Boilerplate](https://github.com/chentsulin/electron-react-boilerplate).

### Dev - Getting Started
* Clone the repo
* Get a [Dark Sky API key](https://darksky.net/dev/account)
* `$ yarn`
* `$ yarn dev`
* Add the appropriate API keys in the "Settings" page
