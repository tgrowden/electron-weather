export const SET_WEATHER_API_KEY: ActionConst = 'SET_WEATHER_API_KEY'
export const SET_RENDER_ICONS: ActionConst = 'RENDER_ICONS'

export function setWeatherApiKey(weatherApiKey: string) {
	return {
		type: SET_WEATHER_API_KEY,
		weatherApiKey
	}
}

export function setRenderIcons(renderIcons: boolean) {
	return {
		type: SET_RENDER_ICONS,
		renderIcons
	}
}
